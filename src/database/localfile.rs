use super::*;
use std::collections::HashMap;
use std::error::Error;
use std::fs::Permissions;
use std::io::{Read, Write};
use std::os::unix::fs::PermissionsExt;
use std::path::{Path, PathBuf};
use std::sync::Mutex;

pub struct LocalFileDatabase {
	dbfile: PathBuf,
	files_dir: PathBuf,
	entries: Mutex<HashMap<FileUuid, UploadMetadata>>,
}

impl LocalFileDatabase {
	const DATABASE_FILES_SUBDIR: &'static str = "files";
	const DBFILE_MODE: u32 = 0o774;
	const UPLOADFILE_MODE: u32 = 0o660;

	pub fn new<P: AsRef<Path> + ?Sized>(dbdir: &P, filename: &P) -> Result<Self, Box<dyn Error>> {
		let entries;
		let dbfile = dbdir.as_ref().join(filename);
		let files_dir = dbdir.as_ref().join(Self::DATABASE_FILES_SUBDIR);
		if !files_dir.exists() {
			std::fs::create_dir(&files_dir)?;
			let perms = PermissionsExt::from_mode(Self::DBFILE_MODE);
			std::fs::set_permissions(&files_dir, perms)?;
		}
		if dbfile.exists() && dbfile.is_file() {
			entries = Self::get_fileuploads(&dbfile)?;
		}
		else {
			entries = HashMap::with_capacity(250);
		}

		Ok(Self {
			dbfile,
			files_dir,
			entries: Mutex::new(entries),
		})
	}

	fn get_fileuploads(
		p: impl AsRef<Path>,
	) -> Result<HashMap<FileUuid, UploadMetadata>, Box<dyn Error>> {
		let rdr = std::fs::File::open(p)?;
		let v = serde_cbor::de::from_reader(rdr)?;

		Ok(v)
	}

	fn save_fileuploads(&self) -> Result<(), Box<dyn Error>> {
		let perms = PermissionsExt::from_mode(Self::DBFILE_MODE);
		let mut wrt = std::fs::File::create(self.dbfile.as_path())?;
		std::fs::set_permissions(self.dbfile.as_path(), perms)?;

		serde_cbor::ser::to_writer(&mut wrt, &*self.entries.lock().unwrap())?;
		Ok(())
	}

	fn check_path(p: &Path) -> bool {
		return p.exists() && !p.is_dir();
	}
}

impl Database for LocalFileDatabase {
	fn delete_expired_files(&self, threshold: u64) -> Result<usize, ApiError> {
		let entries = &mut *self.entries.lock().unwrap();
		let mut count = 0;

		let mut error = None;

		entries.retain(|key, x| {
			if x.should_file_be_removed(threshold) {
				match std::fs::remove_file(self.files_dir.as_path().join(&hex::encode(key))) {
					Ok(_) => {}
					Err(e) => error = Some(e),
				}
				count += 1;
				return false;
			}
			return true;
		});

		match error {
			Some(e) => return Err(e.into()),
			None => {}
		}

		Ok(count)
	}

	fn save_file(
		&self,
		uuid: &FileUuid,
		reader: &mut dyn Read,
		mime: Option<Mime>,
	) -> Result<(), ApiError> {
		let file_path_full = self.files_dir.as_path().join(&hex::encode(uuid));
		if file_path_full.exists() {
			return Err(ApiError::new(ApiErrorType::Conflict, "File exists".into()));
		}

		//create new file
		let file = match std::fs::File::create(&file_path_full) {
			Ok(w) => w,
			Err(e) => return Err(format!("Error creating new file: {}", e).into()),
		};

		match file.set_permissions(Permissions::from_mode(Self::UPLOADFILE_MODE)) {
			Ok(w) => w,
			Err(e) => return Err(format!("Error setting permissions. {}", e).into()),
		}

		//open a file for the upload
		let mut writer = std::io::BufWriter::new(file);
		//write the body to file
		match std::io::copy(reader, &mut writer) {
			Ok(_) => {}
			Err(e) => return Err(format!("Error copying buffer. {}", e).into()),
		};

		match writer.flush() {
			Ok(_) => {}
			Err(e) => return Err(format!("Error flushing writer. {}", e).into()),
		};

		let upload = UploadMetadata::new(mime);
		(*self.entries.lock().unwrap()).insert(*uuid, upload);
		Ok(())
	}

	fn retrieve_file(&self, uuid: &FileUuid) -> Result<Option<Upload>, ApiError> {
		let file_path_full = self.files_dir.as_path().join(&hex::encode(uuid));
		match Self::check_path(&file_path_full) {
			true => match std::fs::File::open(file_path_full) {
				Ok(mut r) => {
					let (size, metadata) = match self.get_upload_metadata(&uuid)? {
						Some(v) => v,
						None => return Ok(None),
					};
					let mut vec = Vec::with_capacity(size as usize);
					r.read_to_end(&mut vec)?;
					Ok(Some(Upload::new(vec.into_boxed_slice(), metadata.clone())))
				}
				Err(e) => Err(e.into()),
			},
			false => Ok(None),
		}
	}

	fn get_upload_metadata(
		&self,
		uuid: &FileUuid,
	) -> Result<Option<(u64, UploadMetadata)>, ApiError> {
		let upload = match (*self.entries.lock().unwrap()).get(uuid) {
			Some(v) => v.clone(),
			None => return Ok(None),
		};

		let file_path_full = self.files_dir.as_path().join(&hex::encode(uuid));
		match Self::check_path(&file_path_full) {
			true => {
				let meta = file_path_full.metadata()?;
				Ok(Some((meta.len(), upload)))
			}
			false => Ok(None),
		}
	}
}

impl Drop for LocalFileDatabase {
	fn drop(&mut self) {
		//save database file
		if let Err(e) = self.save_fileuploads() {
			panic!("Error writing database to file '{:?}': {}", self.dbfile, e);
		}
	}
}
