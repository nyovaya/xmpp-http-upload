#[cfg(feature = "localfiledb")]
mod localfile;

#[cfg(feature = "sqlite")]
mod sqlite;

#[cfg(feature = "postgres")]
mod postgres;

use crate::config::{DatabaseConfig, ServerDatabaseType};
use chrono::offset::Utc;
use chrono::DateTime;
use mime::{FromStrError, Mime};
use mime_serde_shim::Wrapper as MimeWrapper;
#[cfg(feature = "localfiledb")]
use serde::*;
use std::error::Error;
use std::io::Read;
use std::net::SocketAddr;
use std::num::ParseIntError;
use std::sync::Arc;
use std::{fmt, io};

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ApiErrorType {
	Conflict,
	Internal,
}

#[derive(Debug)]
pub struct ApiError {
	_type: ApiErrorType,
	error: Box<dyn Error>,
	desc: String,
}

impl fmt::Display for ApiError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.error.to_string())
	}
}

impl ApiError {
	fn new(_type: ApiErrorType, error: Box<dyn Error>) -> Self {
		let desc = error.to_string();
		Self { _type, error, desc }
	}

	pub fn get_type(&self) -> ApiErrorType {
		self._type
	}
}

impl From<io::Error> for ApiError {
	fn from(error: io::Error) -> Self {
		ApiError::new(ApiErrorType::Internal, Box::new(error))
	}
}

impl From<&str> for ApiError {
	fn from(error: &str) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<String> for ApiError {
	fn from(error: String) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

#[cfg(feature = "sqlite")]
impl From<::sqlite::Error> for ApiError {
	fn from(error: ::sqlite::Error) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<FromStrError> for ApiError {
	fn from(error: FromStrError) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<chrono::ParseError> for ApiError {
	fn from(error: chrono::ParseError) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<ParseIntError> for ApiError {
	fn from(error: ParseIntError) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

#[cfg(feature = "postgres")]
impl From<libpq::errors::Error> for ApiError {
	fn from(error: libpq::errors::Error) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<std::ffi::NulError> for ApiError {
	fn from(error: std::ffi::NulError) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<std::ffi::FromVecWithNulError> for ApiError {
	fn from(error: std::ffi::FromVecWithNulError) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<std::str::Utf8Error> for ApiError {
	fn from(error: std::str::Utf8Error) -> Self {
		ApiError::new(ApiErrorType::Internal, error.into())
	}
}

impl From<Box<dyn Error>> for ApiError {
	fn from(error: Box<dyn Error>) -> Self {
		ApiError::new(ApiErrorType::Internal, error)
	}
}

impl Error for ApiError {
	fn description(&self) -> &str {
		&self.desc
	}
}

type FileUuid = [u8; 32];

pub struct Upload {
	content: Box<[u8]>,
	metadata: UploadMetadata,
}

impl Upload {
	fn new(content: Box<[u8]>, metadata: UploadMetadata) -> Self {
		Self { content, metadata }
	}

	pub fn get_content(&self) -> &[u8] {
		&self.content[..]
	}

	pub fn get_metadata(&self) -> &UploadMetadata {
		&self.metadata
	}
}

#[cfg_attr(feature = "localfiledb", derive(Serialize, Deserialize))]
#[derive(Clone)]
pub struct UploadMetadata {
	time: DateTime<Utc>,
	mime: Option<MimeWrapper>,
	//UUID for V3
	//user: Option<u128>
}

impl UploadMetadata {
	fn new(mime: Option<Mime>) -> Self {
		Self {
			time: Utc::now(),
			mime: mime.map(|m| MimeWrapper(m)),
		}
	}

	fn with_time(mime: Option<Mime>, time: DateTime<Utc>) -> Self {
		Self {
			time,
			mime: mime.map(|m| MimeWrapper(m)),
		}
	}

	#[cfg(feature = "localfiledb")]
	pub fn should_file_be_removed(&self, max_diff: u64) -> bool {
		return Utc::now().signed_duration_since(&self.time).num_seconds() > max_diff as i64;
	}

	pub fn get_mime(&self) -> Option<&Mime> {
		self.mime.as_ref().map(|m| &m.0)
	}

	//V3
	//pub fn get_user_id() -> Option<u128>{ self.user }
}

pub trait Database: Sync + Send {
	fn save_file(
		&self,
		uuid: &FileUuid,
		data: &mut dyn Read,
		mime: Option<Mime>,
	) -> Result<(), ApiError>;

	fn delete_expired_files(&self, threshold: u64) -> Result<usize, ApiError>;

	fn retrieve_file(&self, uuid: &FileUuid) -> Result<Option<Upload>, ApiError>;

	// Size of content + UploadMetadata
	fn get_upload_metadata(
		&self,
		uuid: &FileUuid,
	) -> Result<Option<(u64, UploadMetadata)>, ApiError>;
}

pub struct DatabaseHelper {}

impl DatabaseHelper {
	pub fn create_from_config(
		config: &DatabaseConfig,
	) -> Result<Arc<dyn Database>, Box<dyn Error>> {
		match config {
			DatabaseConfig::File(fc) => match fc.filename.extension() {
				Some(ext) => {
					if ext == "sqlite" {
						#[cfg(not(feature = "sqlite"))]
						return Err("SQLite Backend not supported yet.".into());
						#[cfg(feature = "sqlite")]
						return Ok(Arc::new(sqlite::SqliteDatabase::new(
							fc.path.join(&fc.filename),
						)?));
					}
					#[cfg(feature = "localfiledb")]
					return Ok(Arc::new(localfile::LocalFileDatabase::new(
						&fc.path,
						&fc.filename,
					)?));
					#[cfg(not(feature = "localfiledb"))]
					return Err("No supported database found".into());
				}
				None => return Err("Local database's file extension can't be empty!".into()),
			},
			#[cfg(any(feature = "postgres"))]
			DatabaseConfig::Server(sc) => {
				let addr = SocketAddr::new(sc.address, sc.port);
				match sc.server_type {
					#[cfg(feature = "postgres")]
					ServerDatabaseType::PostgreSql => {
						return Ok(Arc::new(postgres::PostgresDatabase::new(
							addr,
							&sc.username,
							&sc.password,
							&sc.database,
						)?))
					}
					#[cfg(not(any(feature = "postgres")))]
					_ => {
						return Err(format!(
							"Server database '{:?}' is not supported by the compiled binary.",
							sc.server_type
						)
						.into())
					}
				}
			}
		}
	}
}
