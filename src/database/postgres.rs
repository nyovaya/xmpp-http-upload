use super::*;
use chrono::{DateTime, Duration, NaiveDateTime, TimeZone, Utc};
use libpq::connection::Connection;
use libpq::{state, types, Format, State};
use std::collections::HashMap;
use std::error::Error;
use std::ffi::CString;
use std::mem::MaybeUninit;
use std::net::SocketAddr;
use std::str::FromStr;

#[cfg(feature = "postgres")]
pub struct PostgresDatabase {
	con: Connection,
	overwatch_con: Connection,
	sql_start_date: DateTime<Utc>,
}

#[cfg(feature = "postgres")]
impl PostgresDatabase {
	pub fn new(
		addr: SocketAddr,
		username: &str,
		password: &str,
		db_name: &str,
	) -> Result<Self, Box<dyn Error>> {
		let mut params = HashMap::new();
		let host = addr.ip().to_string();
		let port = addr.port().to_string();
		params.insert("host", host.as_str());
		params.insert("port", port.as_str());
		params.insert("dbname", db_name);
		params.insert("user", username);
		params.insert("password", password);
		let con = Connection::with_params(&params, false)?;
		let res = con.prepare(None, "CREATE TABLE IF NOT EXISTS fileupload(filename char(64) PRIMARY KEY, data bytea NOT NULL, mime varchar(255), created timestamp NOT NULL)", &[]);

		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}

		let res = con.exec_prepared(None, &[], &[], Format::Binary);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}

		//Prepare all statements
		let res = con.prepare(
			Some("save_file"),
			"INSERT INTO fileupload(filename, data, mime, created) VALUES ($1::TEXT, $2::BYTEA, $3::TEXT, $4::TIMESTAMP);",
			&[
				types::TEXT.oid,
				types::BYTEA.oid,
				types::TEXT.oid,
				types::TIMESTAMP.oid,
			],
		);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}

		let overwatch_con = Connection::with_params(&params, false)?;

		let res = overwatch_con.prepare(
			Some("delete_expired_files"),
			"DELETE FROM fileupload WHERE created > (created + $1::INT8 * interval '1 second');",
			&[types::INT8.oid],
		);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}
		let res = con.prepare(
			Some("retrieve_file"),
			"SELECT data FROM fileupload WHERE filename = $1::TEXT;",
			&[types::TEXT.oid],
		);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}
		let res = con.prepare(
			Some("get_upload_metadata"),
			"SELECT LENGTH(data), created, mime FROM fileupload WHERE filename = $1::TEXT;",
			&[types::TEXT.oid],
		);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}

		let sql_start_date = Utc.from_utc_datetime(&NaiveDateTime::parse_from_str(
			"2000-01-01 00:00:00.000",
			"%Y-%m-%d %H:%M:%S%.f",
		)?);

		Ok(Self {
			con,
			overwatch_con,
			sql_start_date,
		})
	}
}

unsafe impl Sync for PostgresDatabase {}

fn bytes_to_i64(slice: &[u8]) -> Result<i64, Box<dyn Error>> {
	unsafe {
		let mut array: MaybeUninit<[u8; 8]> = MaybeUninit::uninit();
		if slice.len() != 8 {
			return Err("Slice is not the size of 8".into());
		}
		array.assume_init_mut().copy_from_slice(&slice[0..8]);
		Ok(i64::from_be_bytes(array.assume_init_read()))
	}
}

fn datetime_from_sql_bytes(
	dt: DateTime<Utc>,
	slice: &[u8],
) -> Result<DateTime<Utc>, Box<dyn Error>> {
	return Ok(dt + Duration::microseconds(bytes_to_i64(slice)?));
}

fn current_sql_timestamp(dt: DateTime<Utc>) -> Result<[u8; 8], Box<dyn Error>> {
	match (Utc::now() - dt).num_microseconds() {
		Some(us) => Ok(us.to_be_bytes()),
		None => Err("Overflow while converting Duration to microseconds".into()),
	}
}

fn bytes_to_u32(slice: &[u8]) -> Result<u32, Box<dyn Error>> {
	unsafe {
		let mut array: MaybeUninit<[u8; 4]> = MaybeUninit::uninit();
		if slice.len() != 4 {
			return Err("Slice is not the size of 4".into());
		}
		array.assume_init_mut().copy_from_slice(&slice[0..4]);
		Ok(u32::from_be_bytes(array.assume_init_read()))
	}
}

impl Database for PostgresDatabase {
	fn save_file(
		&self,
		uuid: &FileUuid,
		data: &mut dyn Read,
		mime: Option<Mime>,
	) -> Result<(), ApiError> {
		let mut bytes = Vec::new();
		data.read_to_end(&mut bytes)?;

		let mime_val = match mime {
			Some(v) => Some(CString::new(v.essence_str())?.into_bytes_with_nul()),
			None => None,
		};

		let values = [
			Some(
				CString::new(hex::encode(uuid))
					.unwrap()
					.into_bytes_with_nul(),
			),
			Some(bytes),
			mime_val,
			Some(current_sql_timestamp(self.sql_start_date)?[..].to_vec()),
		];
		let res = self.con.exec_prepared(
			Some("save_file"),
			&values[..],
			&[Format::Text, Format::Binary, Format::Text, Format::Binary],
			Format::Binary,
		);
		if let Some(e) = res.error_message()? {
			match res
				.error_field(libpq::result::ErrorField::Sqlstate)?
				.map(|x| State::from_code(x))
			{
				Some(v) => {
					if v == state::UNIQUE_VIOLATION {
						return Err(ApiError::new(
							ApiErrorType::Conflict,
							"Filename already exists".into(),
						));
					}
				}
				None => {}
			}
			return Err(e.into());
		}
		Ok(())
	}

	fn delete_expired_files(&self, threshold: u64) -> Result<usize, ApiError> {
		let res = self.overwatch_con.exec_prepared(
			Some("delete_expired_files"),
			&[Some(threshold.to_be_bytes()[..].to_vec())],
			&[Format::Binary],
			Format::Binary,
		);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}
		Ok(res.ntuples())
	}

	fn retrieve_file(&self, uuid: &FileUuid) -> Result<Option<Upload>, ApiError> {
		let (_, metadata) = match self.get_upload_metadata(uuid)? {
			Some(v) => v,
			None => return Ok(None),
		};
		let res = self.con.exec_prepared(
			Some("retrieve_file"),
			&[Some(CString::new(hex::encode(uuid))?.into_bytes_with_nul())],
			&[Format::Text],
			Format::Binary,
		);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}
		else if res.ntuples() > 0 {
			match res.value(0, 0) {
				Some(data) => {
					return Ok(Some(Upload::new(Box::from(data), metadata)));
				}
				None => Ok(None),
			}
		}
		else {
			return Ok(None);
		}
	}

	// Size of content + UploadMetadata
	fn get_upload_metadata(
		&self,
		uuid: &FileUuid,
	) -> Result<Option<(u64, UploadMetadata)>, ApiError> {
		let res = self.con.exec_prepared(
			Some("get_upload_metadata"),
			&[Some(CString::new(hex::encode(uuid))?.into_bytes_with_nul())],
			&[Format::Text],
			Format::Binary,
		);
		if let Some(e) = res.error_message()? {
			return Err(e.into());
		}
		else if res.ntuples() > 0 {
			let len = match res.value(0, 0) {
				Some(v) => bytes_to_u32(v)?,
				None => return Err("'length(data)' was unexpectedly null.".into()),
			};
			let created = match res.value(0, 1) {
				Some(v) => datetime_from_sql_bytes(self.sql_start_date, v)?,
				None => return Err("'created' was unexpectedly null.".into()),
			};
			let mime = match res.value(0, 2) {
				Some(v) => Some(Mime::from_str(CString::new(v.to_vec())?.to_str()?)?),
				None => None,
			};
			return Ok(Some((len as u64, UploadMetadata::with_time(mime, created))));
		}
		else {
			return Ok(None);
		}
	}
}
