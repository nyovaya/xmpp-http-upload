use super::*;
use ::sqlite::{Connection, ConnectionThreadSafe, State};
use std::error::Error;
use std::path::Path;
use std::str::FromStr;

pub struct SqliteDatabase {
	con: ConnectionThreadSafe,
	prefix: String,
}

impl SqliteDatabase {
	pub fn new<P: AsRef<Path>>(file: P) -> Result<Self, Box<dyn Error>> {
		let con = Connection::open_thread_safe(file)?;
		let prefix = env!("CARGO_PKG_NAME").replace('-', "_");
		{
			let mut statement = con.prepare(format!("CREATE TABLE IF NOT EXISTS {}_fileupload(filename varchar(64) PRIMARY KEY, data blob NOT NULL, mime varchar(255), created datetime NOT NULL)", prefix))?;
			statement.next()?;
		}
		//Dont synchronously save data to disk directly - Improves the performance when INSERTing new records
		con.execute("PRAGMA synchronous = OFF")?;
		Ok(Self { con, prefix })
	}
}

impl Database for SqliteDatabase {
	fn save_file(
		&self,
		uuid: &FileUuid,
		data: &mut dyn Read,
		mime: Option<Mime>,
	) -> Result<(), ApiError> {
		let mut statement = self.con.prepare(format!("INSERT INTO {}_fileupload(filename, data, mime, created) VALUES (:file, :data, :mime, :datetime);", self.prefix))?;
		let mut v = Vec::new();
		data.read_to_end(&mut v)?;
		statement.bind((":file", hex::encode(uuid).as_str()))?;
		statement.bind((":data", &v[..]))?;
		match mime {
			Some(m) => statement.bind((":mime", m.essence_str()))?,
			None => statement.bind((":mime", None::<&str>))?,
		}
		statement.bind((":datetime", Utc::now().to_string().as_str()))?;
		match statement.next() {
			Ok(_) => {}
			Err(e) => {
				if e.code == Some(19) {
					return Err(ApiError::new(
						ApiErrorType::Conflict,
						"Filename already exists".into(),
					));
				}
			}
		}
		Ok(())
	}

	fn delete_expired_files(&self, threshold: u64) -> Result<usize, ApiError> {
		let mut statement = self.con.prepare(format!(
			"DELETE FROM {}_fileupload WHERE created > DATETIME(created, '+:secs seconds') ;",
			self.prefix
		))?;
		statement.bind(("dt", threshold.to_string().as_str()))?;
		statement.next()?;
		Ok(self.con.change_count())
	}

	fn retrieve_file(&self, uuid: &FileUuid) -> Result<Option<Upload>, ApiError> {
		let (_, metadata) = match self.get_upload_metadata(uuid)? {
			Some(v) => v,
			None => return Ok(None),
		};
		let mut statement = self.con.prepare(format!(
			"SELECT data FROM {}_fileupload WHERE filename = :uuid;",
			self.prefix
		))?;
		statement.bind((":uuid", hex::encode(uuid).as_str()))?;
		let state = statement.next()?;
		match state {
			State::Row => {
				let data = statement.read::<Vec<u8>, usize>(0)?;
				return Ok(Some(Upload::new(data.into_boxed_slice(), metadata)));
			}
			_ => return Ok(None),
		}
	}

	fn get_upload_metadata(
		&self,
		uuid: &FileUuid,
	) -> Result<Option<(u64, UploadMetadata)>, ApiError> {
		let mut statement = self.con.prepare(format!(
			"SELECT LENGTH(data), created, mime FROM {}_fileupload WHERE filename = :uuid;",
			self.prefix
		))?;
		statement.bind((":uuid", hex::encode(uuid).as_str()))?;
		let state = statement.next()?;
		match state {
			State::Row => {
				let len_raw = statement.read::<String, usize>(0)?;
				let len = u64::from_str(&len_raw)?;
				let created_raw = statement.read::<String, usize>(1)?;
				let created = DateTime::<Utc>::from_str(&created_raw)?;
				let mime = statement.read::<Option<String>, usize>(2)?;
				match mime {
					Some(m) => {
						let mime = Mime::from_str(&m)?;
						return Ok(Some((len, UploadMetadata::with_time(Some(mime), created))));
					}
					None => return Ok(Some((len, UploadMetadata::with_time(None, created)))),
				}
			}
			_ => return Ok(None),
		}
	}
}
