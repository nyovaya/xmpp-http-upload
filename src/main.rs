mod args;
mod config;
mod database;
mod misc;
mod overwatch;
mod request;
mod server;

use crate::server::*;

fn main() {
	match XmppUploadServer::new() {
		Ok(server) => server.run(),
		Err(e) => eprintln!("Error: {}", e),
	};
}
