use http::header::HeaderName;
use std::io::Cursor;
use tiny_http::{Header, Response, ResponseBox, StatusCode};

pub trait GetHeaderValue {
	fn get_value(&self, header: HeaderName) -> Option<&str>;
}

impl GetHeaderValue for [Header] {
	fn get_value(&self, header: HeaderName) -> Option<&str> {
		for h in self.iter() {
			if h.field
				.as_str()
				.as_str()
				.eq_ignore_ascii_case(header.as_str())
			{
				return Some(h.value.as_str());
			}
		}
		return None;
	}
}

pub trait FormatStatusCode
where Self: Sized {
	fn format(code: u16) -> Self;
}

impl FormatStatusCode for ResponseBox {
	fn format(code: u16) -> Self {
		let status = StatusCode::from(code);
		let data = Cursor::new(
			format!("<h1>{} - {}</h1>\n", code, status.default_reason_phrase())
				.as_bytes()
				.to_vec(),
		);
		let len = Some(data.get_ref().len() as usize);

		Response::new(status, vec![], data, len, None).boxed()
	}
}
