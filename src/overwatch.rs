use std::sync::{
	atomic::{AtomicBool, Ordering},
	Arc,
};
use std::time::Duration;

use crate::database::Database;
use loglite::*;

pub fn overwatch_handler(
	database: Arc<dyn Database>,
	exp_time: u64,
	log: Log,
	exit: Arc<AtomicBool>,
) {
	std::thread::spawn(move || {
		loop {
			//SIGTERM received
			if exit.load(Ordering::Relaxed) {
				break;
			}
			else {
				match database.delete_expired_files(exp_time) {
					Ok(v) => {
						if v > 0 {
							log.info(format!(
								"Removed '{}' file(s) exceeding the expiration time.",
								v
							))
						}
					}
					Err(e) => log.error(format!("Couldn't remove files. {}", e)),
				}
			}

			std::thread::sleep(Duration::from_secs(1));
		}
	});
}
