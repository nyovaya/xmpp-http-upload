use crate::config::*;
use crate::database::*;
use crate::misc::*;
use http::{header, HeaderMap, HeaderValue};
use loglite::*;
use qstring::QString;
use sha2::{Digest, Sha256};
use std::str::FromStr;
use std::sync::Arc;
use tiny_http::*;
use url::Url;

const MIME_BYTE_STREAM: &'static str = "application/octet-stream";
const DEFAULT_SOURCE: &'static str = "\"default-src 'none'\"";
const ATTACHMENT: &'static str = "attachment";

pub fn process_request(
	req: &mut Request,
	cfg: &Config,
	log: &Log,
	database: Arc<dyn Database>,
) -> ResponseBox {
	let urlstr = format!(
		"http://{}:{}{}",
		cfg.server.address,
		cfg.server.port,
		req.url()
	);
	let url = match Url::parse(&urlstr) {
		Ok(u) => u,
		Err(e) => {
			log.error(format!("Error parsing url '{}'. {}", urlstr, e));
			return FormatStatusCode::format(500);
		}
	};

	let raw_filename = match percent_encoding::percent_decode(url.path().as_bytes()).decode_utf8() {
		Ok(st) => st,
		Err(e) => {
			log.error(&format!("Error converting url to UTF-8: {}", e));
			return FormatStatusCode::format(400);
		}
	};

	// Removes the '/' at the beginning
	let filename = match raw_filename.starts_with('/') {
		true => &raw_filename[1..],
		false => raw_filename.as_ref(),
	};

	let uuid = hash_string(&filename);

	match *req.method() {
		Method::Get | Method::Head => {
			log.info(format!(
				"[{}] Request for file '{}'",
				*req.method(),
				filename
			));

			if *req.method() == Method::Get {
				let upload = match database.retrieve_file(&uuid) {
					Ok(r) => match r {
						Some(v) => v,
						None => {
							log.warning(format!("Resource does not exist '{}'", filename));
							return FormatStatusCode::format(404);
						}
					},
					Err(e) => {
						log.error(format!("Tried to read from file: {}", e));
						return FormatStatusCode::format(500);
					}
				};

				let mut response = Response::from_data(upload.get_content());
				match add_header_to_response(&mut response, create_http_header_get(&upload)) {
					Ok(_) => {}
					Err(_) => {
						log.error("Error creating header.");
						return FormatStatusCode::format(500);
					}
				}
				return response.boxed();
			}
			else {
				let header = match create_http_header_head(database.as_ref(), &uuid, &log) {
					Ok(r) => r,
					Err(e) => return e,
				};
				let mut response = Response::empty(200).with_chunked_threshold(usize::MAX);
				match add_header_to_response(&mut response, header) {
					Ok(_) => {}
					Err(_) => {
						log.error("Error creating header.");
						return FormatStatusCode::format(500);
					}
				}
				response.boxed()
			}
		}
		Method::Put => {
			//check MIME type
			let mut mime_opt = None;
			if let Some(mime) = req.headers().get_value(header::CONTENT_TYPE) {
				if let Ok(m) = mime::Mime::from_str(mime) {
					if cfg.is_mime_blacklisted(&m) {
						log.warning(format!("Received file with blacklisted MIME-type: {}", m));
						return FormatStatusCode::format(415);
					}
					mime_opt = Some(m);
				}
			}

			let qs = QString::from(url.query().unwrap_or(""));
			if let Some(ref version) = HttpUploadVersion::determine_from_querystring(&qs) {
				if let Err(e) = get_check_mac(&filename, &qs, version, &req, cfg, &log) {
					return e;
				}

				match database.save_file(&uuid, &mut req.as_reader(), mime_opt) {
					Ok(_) => {}
					Err(e) => {
						if e.get_type() == ApiErrorType::Conflict {
							log.warning(format!("Tried overwriting file '{:?}'", filename));
							return FormatStatusCode::format(409);
						}
						else {
							log.error(e.to_string());
							return FormatStatusCode::format(500);
						}
					}
				};

				log.info("Successfully saved new file.");
				return FormatStatusCode::format(200);
			}
			else {
				log.error("Couldn't find a matching version-string");

				return FormatStatusCode::format(400);
			}
		}
		_ => {
			log.warning(format!("[{}] Received unallowed method", *req.method()));
			FormatStatusCode::format(405)
		}
	}
}

fn add_header_to_response<R: std::io::Read>(
	response: &mut Response<R>,
	mut header: HeaderMap,
) -> Result<(), ()> {
	for h in header.drain() {
		if let Some(headername) = h.0 {
			response.add_header(
				match tiny_http::Header::from_bytes(headername.as_str().as_bytes(), h.1.as_bytes())
				{
					Ok(h) => h,
					Err(e) => return Err(e),
				},
			);
		}
	}
	Ok(())
}

fn create_http_header_get(upload: &Upload) -> HeaderMap {
	//header
	let mut header = HeaderMap::new();
	header.insert(
		header::CONTENT_DISPOSITION,
		HeaderValue::from_static(ATTACHMENT),
	);

	let mut content_type = HeaderValue::from_static(MIME_BYTE_STREAM);

	match upload.get_metadata().get_mime() {
		Some(mime) => match HeaderValue::from_str(mime.essence_str()) {
			Ok(v) => content_type = v,
			Err(_) => {}
		},
		None => {}
	};

	header.insert(header::CONTENT_TYPE, content_type);

	header.insert(header::CONTENT_LENGTH, upload.get_content().len().into());

	header.insert(
		header::CONTENT_SECURITY_POLICY,
		HeaderValue::from_static(DEFAULT_SOURCE),
	);
	header.insert(
		header::X_CONTENT_TYPE_OPTIONS,
		HeaderValue::from_static(DEFAULT_SOURCE),
	);

	header
}

fn create_http_header_head(
	db: &dyn Database,
	uuid: &[u8; 32],
	log: &Log,
) -> Result<HeaderMap, ResponseBox> {
	//header
	let mut header = HeaderMap::new();
	header.insert(
		header::CONTENT_DISPOSITION,
		HeaderValue::from_static(ATTACHMENT),
	);

	let meta = match db.get_upload_metadata(&uuid) {
		Ok(l) => l,
		Err(e) => {
			log.error(format!("Tried to get metadata: {}", e));
			return Err(FormatStatusCode::format(500));
		}
	};

	let mut header_mime = HeaderValue::from_static(MIME_BYTE_STREAM);
	if let Some((size, metadata)) = meta {
		match metadata.get_mime() {
			Some(mime) => {
				if let Ok(mime_value) = HeaderValue::from_str(mime.essence_str()) {
					header_mime = mime_value;
				}
			}
			None => {}
		}

		//write the length of the file into the header
		if let Ok(len) = HeaderValue::from_str(&size.to_string()) {
			header.insert(header::CONTENT_LENGTH, len);
		}
	}

	header.insert(header::CONTENT_TYPE, header_mime);

	header.insert(
		header::CONTENT_SECURITY_POLICY,
		HeaderValue::from_static(DEFAULT_SOURCE),
	);
	header.insert(
		header::X_CONTENT_TYPE_OPTIONS,
		HeaderValue::from_static(DEFAULT_SOURCE),
	);

	Ok(header)
}

fn hash_string(s: &str) -> [u8; 32] {
	let mut hasher = Sha256::new();
	hasher.update(s.as_bytes());
	hasher.finalize().into()
}

fn get_check_mac(
	filename: &str,
	qs: &QString,
	version: &HttpUploadVersion,
	req: &Request,
	cfg: &Config,
	log: &Log,
) -> Result<(), ResponseBox> {
	use hmac::digest::FixedOutput;
	use hmac::{Hmac, Mac};

	//try to decode the MAC from the percentage encoding
	let mac_string_received = hex::decode({
		let s = qs.get(version.as_str()).unwrap_or("");
		//check if the string has a odd number of chars
		if s.chars().count() % 2 > 0 {
			log.info(&format!("Odd length for hex string: {}", s));
			return Err(FormatStatusCode::format(400));
		}
		else {
			s
		}
	});

	//check decoding Result
	let mac_received = match mac_string_received {
		Ok(m) => m,
		Err(e) => {
			log.info(&format!("Failed decoding string: {}.", e));
			return Err(FormatStatusCode::format(400));
		}
	};

	//check MAC length
	if mac_received.len() != Sha256::output_size() {
		log.info(&format!(
			"Received MAC with wrong length: {}.",
			mac_received.len()
		));
		return Err(FormatStatusCode::format(400));
	}

	//get MAC from config's secret
	let mut mac = match Hmac::<Sha256>::new_from_slice(cfg.server.secret_string.as_bytes()) {
		Ok(m) => m,
		Err(e) => {
			log.info(&format!("Error creating MAC-stream. {}", e));
			return Err(FormatStatusCode::format(500));
		}
	};

	let content_length = match req.headers().get_value(header::CONTENT_LENGTH) {
		Some(v) => v,
		None => return Err(FormatStatusCode::format(411)),
	};

	//Version 1
	if *version == HttpUploadVersion::V1 {
		mac.update(format!("{} {}", filename, content_length).as_bytes());
	}
	//Version 2
	else if *version == HttpUploadVersion::V2 {
		let content_type = match req.headers().get_value(header::CONTENT_TYPE) {
			Some(v) => v,
			None => return Err(FormatStatusCode::format(400)),
		};

		mac.update(format!("{}\0{}\0{}", filename, content_length, content_type).as_bytes());
	}

	//compare MAC values
	let mac_calculated = mac.clone().finalize_fixed().to_vec();
	if mac.verify_slice(&mac_received).is_err() {
		log.warning(&format!(
			"Comparing MAC '{:x?}' against '{:x?}' failed.",
			&mac_calculated, mac_received
		));
		return Err(FormatStatusCode::format(403));
	}

	Ok(())
}
