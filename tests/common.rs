use curl::easy::{Easy2, Handler, List};
use curl::easy::{ReadError, WriteError};
use digest::Output;
use hmac::{Hmac, Mac};
use http::header::{self, HeaderMap, HeaderName, HeaderValue};
use mime::Mime;
use nanorand::{BufferedRng, Rng, WyRand};
use percent_encoding::*;
use serde::Deserialize;
use sha2::Sha256;
use std::error::Error;
use std::io::Read;
pub use std::net::*;
use std::sync::{Arc, Mutex, OnceLock};
use unicode_blocks::BASIC_LATIN;

#[derive(Deserialize)]
struct Config {
	pub server: ServerConfig,
}

impl Config {
	pub fn instance() -> &'static Arc<Self> {
		static CONFIG: OnceLock<Arc<Config>> = OnceLock::new();
		CONFIG.get_or_init(|| {
			let data = std::fs::read_to_string("assets/config.toml").unwrap();
			Arc::new(toml::from_str(&data).unwrap())
		})
	}
}

#[derive(Deserialize)]
struct ServerConfig {
	pub address: IpAddr,
	pub port: u16,
	pub secret_string: String,
}

impl ServerConfig {
	pub fn socket_addr(&self) -> SocketAddr {
		SocketAddr::new(self.address, self.port)
	}
}

fn get_rng() -> &'static Arc<Mutex<BufferedRng<WyRand, 8>>> {
	static RNG_BUFFER: OnceLock<Arc<Mutex<BufferedRng<WyRand, 8>>>> = OnceLock::new();

	RNG_BUFFER.get_or_init(|| Arc::new(Mutex::new(BufferedRng::new(WyRand::new()))))
}

struct Upload<'a> {
	data: &'a [u8],
}

impl<'a> Upload<'a> {
	fn new(data: &'a [u8]) -> Self {
		Self { data }
	}
}

impl<'a> Handler for Upload<'a> {
	fn read(&mut self, data: &mut [u8]) -> Result<usize, ReadError> {
		Ok(self.data.read(data).unwrap_or(0))
	}
}

struct HttpBody {
	data: Vec<u8>,
	headers: HeaderMap,
}

impl HttpBody {
	fn new() -> Self {
		Self {
			data: Vec::new(),
			headers: HeaderMap::new(),
		}
	}
}

impl Handler for HttpBody {
	fn write(&mut self, data: &[u8]) -> Result<usize, WriteError> {
		self.data.extend_from_slice(data);
		Ok(data.len())
	}

	fn header(&mut self, data: &[u8]) -> bool {
		match std::str::from_utf8(data) {
			Ok(st) => {
				let split: Vec<_> = st.split(':').collect();
				if split.len() != 2 {
					return true;
				}
				let key = match split[0].parse::<HeaderName>() {
					Ok(v) => v,
					Err(_) => return true,
				};
				let value = match HeaderValue::from_str(&split[1].replace(&['\n', '\r'], "").trim())
				{
					Ok(v) => v,
					Err(e) => return true,
				};
				self.headers.append(key, value);
				return true;
			}
			Err(_) => false,
		}
	}
}

pub fn upload_file(
	file_name: &str,
	content: &[u8],
	version: HttpUploadVersion,
	mime: Option<Mime>,
) -> Result<u32, Box<dyn Error>> {
	let mut handle = Easy2::new(Upload::new(content));
	let hex_key = generate_key(
		version,
		&Config::instance().server.secret_string,
		file_name,
		content.len(),
		mime.clone().unwrap_or(mime::APPLICATION_OCTET_STREAM),
	)?;
	let url = format!(
		"http://{}/{}?{}={}",
		Config::instance().server.socket_addr(),
		percent_encode(file_name.as_bytes(), NON_ALPHANUMERIC).to_string(),
		version.as_str(),
		hex_key
	);
	handle.http_headers(generate_headers(mime, content.len())?)?;
	handle.url(&url)?;
	handle.put(true)?;
	handle.in_filesize(content.len() as u64)?;

	handle.perform()?;
	let code = handle.response_code()?;
	Ok(code)
}

pub fn upload_file_wrong_secret(
	file_name: &str,
	content: &[u8],
	version: HttpUploadVersion,
	mime: Option<Mime>,
) -> Result<u32, Box<dyn Error>> {
	let mut handle = Easy2::new(Upload::new(content));
	let hex_key = generate_key(
		version,
		&generate_file_name("key"),
		file_name,
		content.len(),
		mime.clone().unwrap_or(mime::APPLICATION_OCTET_STREAM),
	)?;
	let url = format!(
		"http://{}/{}?{}={}",
		Config::instance().server.socket_addr(),
		percent_encode(file_name.as_bytes(), NON_ALPHANUMERIC).to_string(),
		version.as_str(),
		hex_key
	);
	handle.http_headers(generate_headers(mime, content.len())?)?;
	handle.url(&url)?;
	handle.put(true)?;
	handle.in_filesize(content.len() as u64)?;

	handle.perform()?;
	let code = handle.response_code()?;
	Ok(code)
}

pub fn upload_file_key_odd_length(
	file_name: &str,
	content: &[u8],
	version: HttpUploadVersion,
	mime: Option<Mime>,
) -> Result<u32, Box<dyn Error>> {
	let mut handle = Easy2::new(Upload::new(content));
	let mut hex_key = generate_key(
		version,
		&Config::instance().server.secret_string,
		file_name,
		content.len(),
		mime.clone().unwrap_or(mime::APPLICATION_OCTET_STREAM),
	)?;
	hex_key.pop();
	let url = format!(
		"http://{}/{}?{}={}",
		Config::instance().server.socket_addr(),
		percent_encode(file_name.as_bytes(), NON_ALPHANUMERIC).to_string(),
		version.as_str(),
		hex_key
	);
	handle.http_headers(generate_headers(mime, content.len())?)?;
	handle.url(&url)?;
	handle.put(true)?;
	handle.in_filesize(content.len() as u64)?;

	handle.perform()?;
	let code = handle.response_code()?;
	Ok(code)
}

pub fn upload_file_key_non256bit(
	file_name: &str,
	content: &[u8],
	version: HttpUploadVersion,
	mime: Option<Mime>,
) -> Result<u32, Box<dyn Error>> {
	let mut handle = Easy2::new(Upload::new(content));
	let mut hex_key = generate_key(
		version,
		&Config::instance().server.secret_string,
		file_name,
		content.len(),
		mime.clone().unwrap_or(mime::APPLICATION_OCTET_STREAM),
	)?;
	hex_key.pop();
	hex_key.pop();
	let url = format!(
		"http://{}/{}?{}={}",
		Config::instance().server.socket_addr(),
		percent_encode(file_name.as_bytes(), NON_ALPHANUMERIC).to_string(),
		version.as_str(),
		hex_key
	);
	handle.http_headers(generate_headers(mime, content.len())?)?;
	handle.url(&url)?;
	handle.put(true)?;
	handle.in_filesize(content.len() as u64)?;

	handle.perform()?;
	let code = handle.response_code()?;
	Ok(code)
}

pub fn upload_file_key_wrong_upload_version(
	file_name: &str,
	content: &[u8],
	version: HttpUploadVersion,
	mime: Option<Mime>,
) -> Result<u32, Box<dyn Error>> {
	let mut handle = Easy2::new(Upload::new(content));
	let hex_key = generate_key(
		version,
		&Config::instance().server.secret_string,
		file_name,
		content.len(),
		mime.clone().unwrap_or(mime::APPLICATION_OCTET_STREAM),
	)?;
	let url = format!(
		"http://{}/{}?v0={}",
		Config::instance().server.socket_addr(),
		percent_encode(file_name.as_bytes(), NON_ALPHANUMERIC).to_string(),
		hex_key
	);
	handle.http_headers(generate_headers(mime, content.len())?)?;
	handle.url(&url)?;
	handle.put(true)?;
	handle.in_filesize(content.len() as u64)?;

	handle.perform()?;
	let code = handle.response_code()?;
	Ok(code)
}

pub fn retrieve_file(file_name: &str) -> Result<(u32, Box<[u8]>, HeaderMap), Box<dyn Error>> {
	let mut handle = Easy2::new(HttpBody::new());
	handle.url(&format!(
		"http://{}/{}",
		Config::instance().server.socket_addr(),
		percent_encode(file_name.as_bytes(), NON_ALPHANUMERIC).to_string()
	))?;
	handle.get(true)?;

	handle.perform()?;
	let code = handle.response_code()?;
	let handler = handle.get_ref();
	Ok((
		code,
		handler.data.clone().into_boxed_slice(),
		handler.headers.clone(),
	))
}

pub fn get_head_file(file_name: &str) -> Result<(u32, HeaderMap), Box<dyn Error>> {
	let mut handle = Easy2::new(HttpBody::new());
	handle.url(&format!(
		"http://{}/{}",
		Config::instance().server.socket_addr(),
		percent_encode(file_name.as_bytes(), NON_ALPHANUMERIC).to_string()
	))?;
	handle.nobody(true)?;

	handle.perform()?;
	let code = handle.response_code()?;
	let handler = handle.get_ref();
	Ok((code, handler.headers.clone()))
}

pub fn generate_file() -> [u8; u16::MAX as usize] {
	let mut arr = [0; u16::MAX as usize];
	let rng = &mut *get_rng().lock().unwrap();
	rng.fill(&mut arr);
	return arr;
}

pub fn generate_file_name(ext: &str) -> String {
	let rng = &mut *get_rng().lock().unwrap();
	let mut arr = ['\0'; 16];
	for idx in 0..arr.len() {
		arr[idx] = char::from_u32(rng.generate_range(BASIC_LATIN.start()..=BASIC_LATIN.end()))
			.unwrap_or(std::char::REPLACEMENT_CHARACTER);
	}

	let string = String::from_iter(&arr[..]);
	let s = string.replace(&['\0', '/'], &std::char::REPLACEMENT_CHARACTER.to_string());
	let s1: String = s.chars().take(s.len() / 2).collect();
	let s2: String = s.chars().skip(s.len() / 2).collect();
	format!("{}/{}.{}", s1, s2, ext)
}

#[derive(Clone, Copy)]
pub enum HttpUploadVersion {
	V1,
	V2,
}

impl HttpUploadVersion {
	pub fn as_str(&self) -> &str {
		match self {
			HttpUploadVersion::V1 => "v",
			HttpUploadVersion::V2 => "v2",
		}
	}
}

fn generate_headers(mime: Option<Mime>, content_len: usize) -> Result<List, Box<dyn Error>> {
	let mut list = List::new();
	list.append(&format!(
		"{}: {}",
		header::CONTENT_TYPE.as_str(),
		mime.unwrap_or(mime::APPLICATION_OCTET_STREAM).essence_str()
	))?;
	Ok(list)
}

fn generate_key(
	version: HttpUploadVersion,
	secret: &str,
	file_name: &str,
	content_len: usize,
	mime: Mime,
) -> Result<String, Box<dyn Error>> {
	let key = match version {
		HttpUploadVersion::V1 => generate_v1_key(secret, file_name, content_len),
		HttpUploadVersion::V2 => generate_v2_key(secret, file_name, content_len, mime),
	}?;
	Ok(format!("{:x}", key))
}

fn generate_v1_key(
	secret: &str,
	file: &str,
	content_len: usize,
) -> Result<Output<Hmac<Sha256>>, Box<dyn Error>> {
	let mut mac = Hmac::<Sha256>::new_from_slice(secret.as_bytes()).unwrap();
	mac.update(&format!("{} {}", file, content_len).as_bytes());
	Ok(mac.finalize().into_bytes())
}

fn generate_v2_key(
	secret: &str,
	file: &str,
	content_len: usize,
	content_type: Mime,
) -> Result<Output<Hmac<Sha256>>, Box<dyn Error>> {
	let mut mac = Hmac::<Sha256>::new_from_slice(secret.as_bytes()).unwrap();
	mac.update(&format!("{}\0{}\0{}", file, content_len, content_type.essence_str()).as_bytes());
	Ok(mac.finalize().into_bytes())
}
